# This file is part of Collapser.

# Collapser is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Collapser is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Collapser.  If not, see <http://www.gnu.org/licenses/>.

META := install.rdf chrome.manifest icon.png icon64.png COPYING README
CONTENT := content/overlay.xul content/collapser.js
SKIN := skin/*.css skin/*.png
LOCALE := locale/en-US/translations.dtd

all: collapser.xpi

collapser.xpi: $(META) $(CONTENT) $(SKIN) $(LOCALE)
	zip -r $@ $^
